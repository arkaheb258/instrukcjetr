### Struktura katalogów
Głównym katalogiem w Linuksie jest / , a w nim znajdują się następujące katalogi (podaję w kolejności alfabetycznej):  
- /bin - tutaj znajdują się podstawowe pliki binarne (wykonywalne),  
- /boot - w tym katalogu znajdują się pliki niezbędne do uruchomienia systemu np. kernel czy pliki GRUBa,  
- /dev - znajdujące się tutaj pliki odnoszą się do urządzeń - za ich pośrednictwem system komunikuje się z urządzeniami (komunikacja niskopoziomowa),  
- /etc - tutaj znajdują się pliki konfiguracyjne i ustawienia systemowe,  
- /home - w tym katalogu znajdują się pliki określające ustawienia każdego użytkownika, 
ponadto jest on przeznaczony na zapisywanie danych, np. dokumentów, obrazków, muzyki i wszelkich plików których używamy na co dzień.  
Przydatną rzeczą może okazać się, zrobienie /home na osobnej partycji, dzięki temu, jeśli z jakiś powodów będziesz musiał zainstalować ponownie system, nie utracisz swoich danych,  
- /lib - tutaj znajdują się systemowe biblioteki dzielone (shared libraries) zawierające funkcje, które są wykonywane przez wiele różnych programów,  
- lost+found - tutaj narzędzia sprawdzające system plików umieszczają zagubione pliki (np. z powodu błędów na dysku),  
- /media - stąd mamy dostęp do np. pendrive, CD-ROM, etc.,  
- /mnt - tutaj natomiast najczęściej montuje się dyski,  
- /proc - wirtualny katalog, zawierający przeróżne dane o systemie i sprzęcie,  
- /root - główny katalog roota (administratora). Jest to swego rodzaju odpowiednik /home,  
- /sbin - katalog ten zawiera pliki wykonywalne poleceń, które mogą być wykonywane tylko przez administratora,  
- /tmp - folder przeznaczony na pliki tymczasowe,  
- /usr - w tym katalogu są instalowane dodatkowe programy, które umożliwiają pracę zwykłemu użytkownikowi systemu. Znajdziesz tam również źródła programów,  
- /var - katalog przeznaczony na pliki, których zawartość często się zmienia. Są tam między innymi przetrzymywane logi programów i systemu.  