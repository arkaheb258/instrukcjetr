# Wersja
v 1.0 - 2015-06-10 - Stworzenie dokumentu.

# Wydawka kombajnu
### Wizualizacja:
- Aktualizacja historii wersji oprogramowania, dodać opisy zmian.  Stworzyć nowe pole json.  
- Aktualizacja plików json z excela - ustawienie odpowiedniej konfiguracji (ukrycie niepotrzebnych buttonów itp).   
- Aktualizacja serwera Arka przed rozpoczęciem testów wizualizacji.  
- Aktualizacja oprogramowania sterownika przed rozpoczęciem testów wizualizacji:  
    - Nowe kody wynikowe - przegrać do katalogu www/plc.  
    - Nowe parametry- przegrać do katalogu www/plc.  
    - Nowe pliki komunikatów i parametrów json  
    - Nowe pliki diagnostyki bloków  
- Ręczne przegranie domyślnych plików jsona do katalogu `jsonDefault`  
- Test oprogramowania przed wydawką:  
    - Ustawienie poziomu dostępu użytkownika na **USER**.  
    - Otworzenie każdej planszy z pomiarów i diagnostyki, sprawdzenie ładowania tekstów.  
    - Zakładanie/zdejmowanie blokad.  
    - Ustawianie parametrów, wszystkie typy (time, string, real, lista).  
    - Wersje językowe - czy dobrze się ładują czcionki rosyjskie itp.  
    - Tab1 - numery wersji programu.  
    - Ustawianie daty i czasu.  
    - Sprawdzenie nawigacji z ramki tcp.  
    - Reakcje na zerwanie komunikacji z plc, serwerem.  
    - Synchronizacja czasu z PLC.  
    - Test zabezpieczeń.  
- Sprawdzenie poprawności plików `jsonDefault`  
- Git,  zrobienie tagu z numerem wersji oraz nazwą kopalni. Nie trzeba robić nowej gałęzi, tag wystarczy.    
To jest tylko mój prywatny backup, oficjalny będzie na serwerze kopexu.

### Instrukcje
- Aktualizacja Git-a.  
- Wygenerowanie instrukcji wewnętrznych dla produkcji w pdf z plików *.md - użyć strony https://gitprint.com/ albo tasków grunta.
- Wygenerowanie aktualnej instrukcji obsługi dla KWK.  

### UPT
- Przegrać aktualne nastawy.  
- Przegrać aktualny plik ze skryptem UPT.  

### Systemy operacyjne
- Link do aktualnego systemu operacyjnego monitora głównego.  
- Link do aktualnego systemu operacyjnego monitora na skrzyni.  

### Kopia lokalna i na serwer
- Wykonać kopie lokalną taskiem grunt.js:   
`$ grunt wydawka`  

- Skopiować serwer na którym były robione testy.  
- Skopiować program sterownika wraz z wszystkimi plikami na którym były robione testy.  

### Kopia na serwer
- Przegrać ręcznie katalog **zrodla** do lokalizacji:  
*192.168.30.12/ProjektyTR1/DRIW/.....*  
- Przegrać ręcznie katalog **produkcja** do lokalizacji:  
*192.168.30.12/Oprogramowanie dla produkcji/Kombajny/.....*  



# Wydawka STD
- Podlinkować miejsce aktualnego systemu operacyjnego.  
- Podlinkować instrukcje.  
    