### Zmiany w systemie
Wgrać na pendrive system dla produkcji. 
Po uruchomieniu sformatować sda1.  
Ustawić ip na dhcp.  
Zainstalować ezremaster (onDemand).  
Wprowadzić zmiany.  

wprowadzić ip statyczne.  
Stworzyć nowy system za pomocą ezremaster (opisane wyżej).  



### EZremaster   
- Stworzenie systemu dla bootowalnego usb dla produkcji za pomocą narzędzia ezremaster.tcz  
    - Plik obrazu iso powinien mieć nazwe „TinyCore-current”, przy innych nazwach się wysypuje  
    - Boot codes (nie wyciemniać ekranów startowych żeby system się  różnił od docelowego):   
        `vga=789 waitusb=5 tce={sda1}`         
        //waitusb ważne! tce po to aby system uruchamiał się tylko z pendrive, bez automatycznego montowania dysku wewn po znalezieniu na nim struktury plików tce  
    - Path to tgz: mnt/sda1/tce/mydata.tgz  
    - “Outside initrd apps on boot”, zaznaczyć  radiobutton w lewym dolnym rogu (ładowanie aplikacji tcz do pamieci podczas bootowania).   
        Ta metoda jest najlepsza bo niczego nie ładuje do jądra, można łatwo usuwać programy itp.  
        Przy innych opcjach za cholere nie wiadomo potem jak usunąć wbar i inne programy.  
    - Skopiować obraz  ezremaster.iso na pendrive (tutaj będą tylko te programy które są uruchamiane w trybie OnBoot, trzeba skopiować resztę)  
