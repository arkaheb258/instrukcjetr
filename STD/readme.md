# Wskazówki 
Podpowiedzi dla podstawowych problemów podczas tworzenia systemu.


### Możliwość wyboru dysku w DC1 do zainstalowania systemu
Dodać komponent „Bootable Windows USB Stack” – bez tego podczas instalacji gdy dojdzie do wyboru dysku na którym ma być zainstalowany system nie będzie żadnej opcji – wszystkie dyski będą niewidoczne.

### Ustawienia języka i klawiatury
International-Core-WinPE   
1 WindowsPE
>InputLocale:    pl-PL  
SystemLocal:    en-US  
UILanguage:     en-US  
UserLocal:      en-US  
WillShowUI:     Never  
UILanguage:     en-US  

### Brak wyświetlania warunków licencyjnych
Setup_x86  
1 WindowsPE  
>UserDada:      AcceptEula: True

### Usprawnienie działania EWF
Core-Settings_x86  
2 Offline Servicing  
>EnablePrefetcher:   0  
EnableSuperfetch:   0

### Usprawnienia dla komponentu „Bootable Windows USB stack„
Kernel-PnP_x86  
2 Offline Servicing  
>BootDriverFlags:            4 (??? System żąda 6)  
PollBootPartitionTimeout:   15000


### Tła Kopexu podczas bootowania systemu
Custom Logon Desktop Background Images  
4 Specialize  
>Path:   C:\Program Files (x86)\Windows Embedded Standard 7\DSSP1\Obrazki Startup  

Do katalogu Obrazki Startup wgrać pliki z tapetami. Nazwy plików mają wyglądać następująco:  
backgroundDefault.jpg  
background768×1280.jpg  
background900×1440.jpg  
background960×1280.jpg  
background1024×1280.jpg  
background1280×1024.jpg  
background1024×768.jpg  
background1280×960.jpg  
background1600×1200.jpg  
background1440×900.jpg  
background1920×1200.jpg  
background1280×768.jpg  
background1360×768.jpg  
Plik backgroundDefault.jpg będzie użyty jeśli system nie znajdzie pliku z nazwą o odpowiedniej rozdzielczości. Po instalacji na docelowym urządzeniu pliki będą się znajdować w C:\\windows\system32\oobe\info\backgrounds.


### Licencja 
Shell-Setup_x86  
4 Specialize  
>ProductKey: VWQMV-YJDC8-9QKDW-2RQTP-F9FQ8


### Nazwa komuptera
Shell-Setup_x86  
4 Specialize  
>ComputerName:   KopexMachinery

Uwaga: Trzeba ustawić nazwę bo inaczej podczas instalacji trzeba będzie to wpisywać ręcznie


### Ustawienie swojej tapety
Shell-Setup_x86  
4 Specialize  
>ThemeName:         logo1.theme  
DesktopBackground:  C:\STD\Tapety\logo1.jpg  

UWAGA: W DesktopBackground ustawić lokalizację pliku z tapetą – ale ma to być lokalizacja na komputerze docelowym a nie na tym na którym się tworzy system w ICE!)


### Informacje o producencie
Shell-Setup_x86  
4 Specialize  
>OEMInformation: Manufacturer: Kopex Machinery


### Wyłączenie Firewalla
Networking-MPSSVC-Svc  
4 Specialize  
>DomainProfile_EnableFirewall:   False  
PrivateProfile_EnableFirewall:  False  
PublicProfile_EnableFirewall:   False  


### Przywracanie systemu - wyłączenie
SystemRestore-Main_x86  
4 Specialize  
>DisableSR:  1


### EWF
Enhanced Write Filter with HORM  
4 Specialize  
>EwfMode:                RamReg  
ProtectBcdPartition:    True  
ProtectedVolumes:       (Key: 1), (DiskNumber: 0), (PartitionNumber: 2)  
BootStatusPolicy:       IgnoreAllFailures   


### Powerplan – ustawienia podstawowe
powercpl_x86  
4 Specialize  
>PreferredPlan: 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c   

Uwaga:  The preferred power plan (Power Options) is set to "High performance". 


### Ustawienia językowe
International-Core_x86  
7 OOBE System  
>InputLocal:    pl-PL  
SystemLocal:    en-US  
UILanguage:     en-US  
UserLocal:      en-US  


### Usprawnienia systemu od Berneckera
Shell-Setup_x86  
7 OOBE System  
>FirstLogonCommands.SynchrousCommand  
CommandLine: c:\vcredist_x86\vcredist_x86_english /q  
Description: The setup for C++ 2008 Redistributable Package starts silently   

>FirstLogonCommands.SynchrousCommand  
CommandLine: cmd /c rd /s /q c:\vcredist_x86  
Description: The installation files for C++ 2008 Redistributable Package are deleted from the system  

>FirstLogonCommands.SynchrousCommand    
CommandLine: %windir%\regedit.exe /S "C:\Windows\System32\AddWriteFilterManager.reg”  
Description: The Write Filter Manager is registered as a Control Panel applet and reg-istered under the HKLM Run key 

>FirstLogonCommands.SynchrousCommand  
CommandLine: c:\Windows\System32\BrSetup\PrepareSetup.bat  
Description: The batch file PrepareSetup.bat is executed   

Uwaga: Należy dodać do obrazu systemu katalogi wszystkie katalogii $OEM$ od Berneckera oraz KopexMachinery2


### Ustawienie adresu IP
Shell-Setup_x86  
7 OOBE System  
>FirstLogonCommands.SynchrousCommand  
CommandLine: netsh interface ip set address name="Local Area Connection" static 192.168.3.70 255.255.255.0


### Wyłączenie żądania logowania po wyjściu z hibernacji
Shell-Setup_x86  
7 OOBE System  
>FirstLogonCommands.SynchrousCommand  
CommandLine: powercfg -SETDCVALUEINDEX SCHEME_MIN SUB_NONE CONSOLELOCK 0  


### Ustawienia sieci
Shell-Setup_x86  
7 OOBE System  
>OOBE:  
NetworkLocation: Work  
HideWirelessSetupInOOBE: True  
ProtectYourPC: 3  
HideEULAPage: True  


### Autologowanie
Shell-Setup_x86  
7 OOBE System  
>Autologon  
UserName: Administrator  
Enabled: True  
LogonCount: 999  


### Wyłączenie wygaszacza ekranu
Shell-Setup_x86  
7 OOBE System  

>FirstLogonCommands.SynchrousCommand  
Action: AddListItem  
Order: kolejny numer, np. 2  
CommandLine: powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c  
Description: The power plan (Power Options) is set to "High performance" (Min power saving)  

>FirstLogonCommands.SynchrousCommand  
Action: AddListItem  
Order: 3  
CommandLine: powercfg -change -monitor-timeout-ac 0  
Description: The option "Turn off monitor" is set to "Never".  


### Rozdzielczość monitora
Shell-Setup_x86  
7 OOBE System  
>Display  
ColorDepth: 32  
RefreshRate: 60  
HorizontalResolution: 1024  
VerticalResolution: 768  
DPI: 100  


### Konto użytkownika (administratora)
Shell-Setup_x86  
7 OOBE System  
>UserAccounts.LocalAccount  
Name: Administrator  
Group: Administrators  


### Dodatek 1 – nie pamiętam dlaczego to ustawiłem
Shell-Setup_x86  
7 OOBE System  
>BluetoothTaskbarIconEnabled:    False

### Dodatek 2 – nie pamiętam dlaczego to ustawiłem
Shell-Setup_x86  
7 OOBE System  
>DisableAutoDaylightTimeSet:    True

### Strefa czasowa – nie trzeba wybierać podczas instalacji
Shell-Setup_x86  
7 OOBE System  
>TimeZone:  UTC















