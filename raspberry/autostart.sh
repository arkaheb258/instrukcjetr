#!/bin/sh

# Raspberry Pi - autostart
# v1.0 - 2016-01-10 - start
# v1.1 - 2016-01-14 - dodano uruchomienie midori w privateBrowsing

# wylaczenie wygaszania monitora
xset -dpms          # disable DPMS (Energy Star) features.  
xset s off          # disable screen saver  
xset s noblank      # don't blank the video device 

# uruchomienie programu do chowania wskaznika myszy
xdotool mousemove 1024 600 &

# uruchomienie programu do przesuwania wskaznika myszy (w rog ekranu)
sudo unclutter -display :0.0 -idle 1 -root &

# przejscie do lokalizacji serwera
cd /home/zzm/kopex/node

# uruchomienie serwera
node forever.js webServer.js &
node forever.js strada.js --interval=100 &

#sleep 2
# uruchomienie przegladarki  
matchbox-window-manager &
#midori -e Fullscreen -a http://127.0.0.1:8888/index.html --private
midori -e Fullscreen -a /home/zzm/kopex/build/index.html --private
#midori -e Fullscreen -a /home/zzm/kopex/buildNew/index.html


# uruchomienie serwera vnc - bez srodowiska X nie da rady!
#sleep 5
#x11vnc -forever &





