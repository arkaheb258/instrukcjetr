
# Przygotowanie pendrive
Ściągnąć najnowszą wersje linuxa TinyCore (ta wersja ~15Mb wystarczy).  
Wgrać ściągniętą wersję linuxa na bootowalnym pendrivie (np. za pomocą Universal-USB-Installer).  
W linuxie użyć `unetbootin`  
Zainstalować system z pendriva na docelowym komputerze (np. DC1):  


# Instalacja z pendrive  
- Podłączyć monitor do internetu
- W instalatorze wybrać "Default", jeśli wybierze się "Boot TinyCore (on slow devices)" to w jakiś sposób załadują się dane z wewnetrznych dysków.  
- Sformatować dyski:  
`$ mkfs.ext4_/dev/sdb1`  
`$ mkfs.ext4_/dev/sdc1` 
- PPM -> System Tools -> Apps, potem już w otwartm okienku wybrać Apps -> Cloud (Remote) -> Browse -> wybrać tc-install.tcz i zainstalować (OnBoot / Go)  
- Zainstalować system na komputerze – z  paska na dole z aplikacjami kliknąć ikonkę tcInstall   
    - Path to core gz: mnt/sdc1/boot/core.gz  
    - Zaznaczyć whole disk  
    - Sda  
    - Install boot loader ma być zaznaczony  
    - Formatting options: ext4  
    - Boot options reference list: vga=792  
    - Extension installation: mnt/sdc1/cde (należy go zaznaczyć a nie wskazać jego wnętrze)  
    - Review: Proceed  
- Wyciągnąć pendrive-a i zrestartować system (backup options: none) i odpalić już go z dysku wewnętrznego (zmienić ustawienia w biosie).  


# Uruchomienie systemu na DC1
### Instalacja podstawowych programów
- Zainstalować tc-install (onBoot) – wtedy ezremaster doda go do swoich repozytoriów, po zainstalowaniu już systemu przez Matrix przeniesie się go do OnDemand  
- Zainstalować mc – midnight commaner (onBoot) – j.w.  
- Zainstalować ezremaster (onDemand)  
- Zainstalować Chrome (chromium-browser-locale.tcz) (onBoot)  
- Zainstalować dejavu-fonts-ttf.tcz (onBoot)   
- Zainstalowanie programu chowającego wskaźnik myszy unclutter.tcz (onBoot)   
- Zainstalować nodejs.tcz (onBoot)   
- Zainstalować openssh.tcz (onBoot)   


### Serwer SSH
Zmienić nazwy plików w katalogu /usr/local/etc/ssh/  
>sudo cp sshd_config.example sshd_config  
sudo cp ssh_config.example ssh_config`  

Ustawienie aby ustawienia były stałe (po reboocie). W pliku /opt/.filetool.lst dodać następujące wpisy:  
>usr/local/etc/ssh  
etc/passwd  
etc/shadow  

Uruchomienie jednorazowe serwera openssh:    
`sudo /usr/local/etc/init.d/openssh start`  lub  
`sudo ./openssh start`  

Ustawienie hasła dla klienta. W konsoli wpisać komendę  
`$ passwd` I ustawić hasło na **wihajster1234**.  

Zrestartować system i spróbować się połączyć za pomocą putty. Najlepiej użyć adresu ip z routera.  
    
    
### Instalacja VNC
`wget http://www.tinycorelinux.net/4.x/x86/tcz/x11vnc.tcz`   
`wget http://www.tinycorelinux.net/4.x/x86/tcz/x11vnc.tcz.dep`  
`wget http://www.tinycorelinux.net/4.x/x86/tcz/libssl-0.9.8.tcz`  

Pliki do zainstalowania (ściągnięte poprzez wget) są w:  */home/tc*  
Pliki należy przegrać do: */mnt/sda1/tce/optional*  
Apps/Load App Locally - wybrać x11vnc   
Apps/Maintenance/OnBoot Maintenance - przenieść tutaj x11vnc
   
Komendy w konsoli:  
`x11vnc -storepasswd`  - Nadać hasło *wihajster*  

Po restarcie wykonać próbę podłączenia.  


### Instalacja xdotool
Program odpowiada za przesunięcie kursora myszki we wskazany punkt ekranu.  
Ściągnąć pakiet za pomocą wget xdotool.tcz:  
`wget http://www.tinycorelinux.net/4.x/x86/tcz/xdotool.tcz`  
`wget http://www.tinycorelinux.net/4.x/x86/tcz/xdotool.tcz.dep`  
`wget http://www.tinycorelinux.net/4.x/x86/tcz/Xorg-7.6-lib.tcz`  

Pliki do zainstalowania (ściągnięte poprzez wget) są w:  */home/tc*   
Pliki należy przegrać do: */mnt/sda1/tce/optional*  
Apps/Maintenance/OnBoot Maintenance  


### Dodanie tapet pulpitu
Jeżeli przegrywamy z dodatkowego pendrive’a należy najpierw zamontować jego system plików.    
Z dolnego menu wybrać narzędzie MountTool i dodać swój nośnik danych (np. sdd1).
Tapety przegrać z /mnt/np. sdd1 do: /opt/backgrounds.  
Z dolnego menu wybrać narzędzie ControlPanel -> Wallpaper -> Background Image wybrać tło pulpitu.  


### Stworzenie pustego katalogu kopex  
W katalogu home/tc/ stworzyć pusty katalog o nazwie `kopex`  

     
### Ustawienia sieciowe
Z ControlPanel  -> Network  
Interface: eth0  
Use DHCP Broadcast?: no  
IP Adress: 192.168.3.31  
Network Mask: 255.255.255.0  
Broadcast: 192.168.3.255  
Gateway: 192.168.3.1  
Name Servers  
Automatycznie  
Automatycznie  
Save Configuration: yes  
Zatwierdzić przyciskiem Apply.  


### Autostart
Przegrać plik autostartu do katalogu .X.d  
Nadać mu uprawnienia do uruchamiania:
`chmod +x autostart`  
Zapamiętać zmiany  


### Ostatnie testy
Przegrać plik autostart i go uruchomić
Sprawdzić działanie połączenia po sftp, putty i vnc.  
Zrestartować system i zapamiętać wszystkie wprowadzone zmiany.  


### EZremaster   
Stworzenie systemu dla bootowalnego usb dla produkcji za pomocą narzędzia ezremaster.tcz  

- Plik obrazu iso powinien mieć nazwe „TinyCore-current”, przy innych nazwach się wysypuje  
- Boot codes (nie wyciemniać ekranów startowych żeby system się  różnił od docelowego):   
    `vga=792 waitusb=5 tce={sdc1}`         
    //waitusb ważne! tce po to aby system uruchamiał się tylko z pendrive, bez automatycznego montowania dysku wewn po znalezieniu na nim struktury plików tce  
- Path to tgz: mnt/sda1/tce/mydata.tgz  
- “Outside initrd apps on boot”, zaznaczyć  radiobutton w lewym dolnym rogu (ładowanie aplikacji tcz do pamieci podczas bootowania).   
    Ta metoda jest najlepsza bo niczego nie ładuje do jądra, można łatwo usuwać programy itp.  
    Przy innych opcjach za cholere nie wiadomo potem jak usunąć wbar i inne programy.  
- Skopiować obraz  ezremaster.iso na pendrive (tutaj będą tylko te programy które są uruchamiane w trybie OnBoot, trzeba skopiować resztę)  


# Dodatkowe informacje
### Odwrócenie orientacji ekranu o 180 stopni:
Home/tc/*.xsession zmodyfikować 1024x768x32 na 1024x768@180x32


### Restart z lini poleceń
`$ filetool.sh -b`  
`sudo reboot`  
   
   
### Zmiany w systemie
Wgrać na pendrive system dla produkcji. 
Po uruchomieniu sformatować sda1 oraz sdb1.  
Ustawić ip na dhcp.  
Zainstalować ezremaster (onDemand).  
Wprowadzić zmiany.  
wprowadzić ip statyczne.  
Stworzyć nowy system za pomocą ezremaster (opisane wyżej).  















# ------------ STARE ---------------------
### Dodanie tapet pulpitu i pliku autostart
Jeżeli przegrywamy z dodatkowego pendrive’a należy najpierw zamontować jego system plików.    
Z dolnego menu wybrać narzędzie MountTool i dodać swój nośnik danych (np. sdd1).
Tapety przegrać z /mnt/np. sdd1 do: /opt/backgrounds.  
Z dolnego menu wybrać narzędzie ControlPanel -> Wallpaper -> Background Image wybrać tło pulpitu.  

Przegrać plik autostart do katalogu home/tc/.X.d   

UWAGA: Dodać uprawnienia skrypton Arka.  
Znawigować do kopex/scripts.  
`$ chmod +x *`  
`$ chmod +s *`  


### Katalog kopex
Przegrać aktualny serwer i strone z wizualizacją do katalogu /home/tc/kopex 



 
### Instalacja obsługi skryptów ftp  
!!!!!!!!!!!!!!!     
W chwili obecnej to nie jest zrobione!  
`$ sudo apt-get install ncftp`    
`$ sudo npm install -g forever`  
!!!!!!!!!!!!!!!!!!  

