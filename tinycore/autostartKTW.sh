
#!/bin/sh

# KTW - monitor glowny DC1
# v1.0 - 2015-12-03 - stworzenie skryptu


# wylaczenie wygaszania monitora
xset s off
xset -dpms

# przejscie do lokalizacji serwera
cd /home/tc/kopex
#cd /mnt/sda1/tce/kopex

# uruchomienie serwera
node node/forever.js node/strada.js --master=http://192.168.3.51:8888 &
node node/forever.js node/webServer.js &

# uruchomienie serwera vnc // -viewonly
x11vnc -forever -httpdir /usr/local/share/x11vnc/classes/ &

# uruchomienie programu do chowania wskaznika myszy
xdotool mousemove 0 0 &

# uruchomienie przegladarki z wizualizacja
chromium-browser http://127.0.0.1:8888/index.html --window-size=1024,768  -kiosk -incognito &

# uruchomienie programu do przesuwania wskaznika myszy (w rog ekranu)
sudo unclutter -display :0.0 -idle 1 -root &

# uruchominie serwera ssh
sudo /usr/local/etc/init.d/openssh start &



#sudo mount /dev/sdb1 /mnt/sdb1/
#sudo ntpclient -s -c 1 -h 192.168.3.51 &
#sudo hwclock -w &
