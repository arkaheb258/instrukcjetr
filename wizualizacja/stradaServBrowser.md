# Informacje wstępne
Wersja:  
v1.0 - 2015-05-13 - stworzenie dokumentu.   

# Rozkazy istniejące  
Rozkazy wprowadzone do dnia 2015-05-13.

     
### 16#201 - Zapisz datę i czas 
Data epoch - ja wysylam Arkowi taka jaka wpisal uzytkownik -> serwer www przerabia przesuniecia za pomoca dwoch parametrow.    
>{  
    "rozkaz": "ustawCzas",  
    "wartosc": "aaaaaaaaaa"  
}  

### 16#202 - Załóż / zdejmij blokadę
>{  
    "rozkaz": "ustawBlokade",  
    "dostep": "Srvc", //pierwsza litera zawsze z duzej  --> Brak, User, Srvc, Adv  
    "slowo": 5,  
    "bit": 3,  
    "wartosc": 1  
}  

### 16#203 - Zapisz aktualny język
Osługiwane parametrem.  

### 16#500 - Zapisz parametr
>{  
    "rozkaz": "ustawParametr",  
    "typ": "aaaaa", // np. pString  
    "id": "aaaaa", //nazwa parametru ze sterownika np. sKonfblabla  
    "wartosc": "aaaaaa"   
}  

### 16#502 -  Obsluga plików parametrów
Operacje na plikach parametrów (zapis/odczyt dla 5 plikow uzytkownikow), przywrócenie ustawien domyślnych.  
>{  
    "rozkaz": "ustawPlik",  
    "plik": "aaaaaa", // default  
    "akcja": "aaaaa" // load  
}  

### 16#308 - Podaj historię zdarzeń
>{  
    "rozkaz": "podajHistorie"  
}  

### 16#701 - Kalibracja enkoderów
>{  
    "rozkaz": "kalibracja", // identyfikator dla Arka  
    "napedId": 0, // identyfikator napędu  
    "pozycja": 0 // wartość kątowa pozycji napędu -160..160° pomnożona x100 -> ja wysylam to co wpisal uzytkownik np 25.6 (nie wymnazam)  
}  		 			
	
### 16#702 - Ustawianie liczników czasu pracy dla kombajnów chodnikowych
>{  
    "rozkaz": "liczniki",  
    "rozkazId": 1,   
    "wartosc": 0  
}  














# Brakujące rozkazy STRADA
Dopisanie reszty rozkazów z protokołu STRADA.  

### 16#204 Zapisz aktualny numer sekcji
**UWAGA: Ten rozkaz niedawno uległ zmianie w STRADA**
>{  
    "rozkaz": "nrSekcji_204"   // identyfikator dla Arka   
    "wWartosc": 0,  // numer sekcji
    "wID": 0  
}

### 16#207 - Zapisz miejsce sterowanie posuwem
>{  
    "rozkaz": "miejsceSterPosuw_207", // identyfikator dla Arka   
    "wWartosc": 0  
}  

### 16#208 - Zapisz tryb pracy posuwu
**UWAGA: Ten rozkaz niedawno uległ zmianie w STRADA**  
>{  
    "rozkaz": "trybPracyPosuw_208",  
    "wWartosc": 0  
}  


### 16#209 - Zapisz tryb pracy ciągników
**UWAGA: Ten rozkaz niedawno uległ zmianie w STRADA**  
>{  
    "rozkaz": "trybPracyCiagniki_209",  // identyfikator dla Arka  
    "wWartosc": 0  
}  

### 16#20A - Zapisz całkowity czas pracy kombajnu
**UWAGA: Ten rozkaz niedawno uległ zmianie w STRADA**  
>{  
    "rozkaz": "czasPracy_20A",    // identyfikator dla Arka  
    "wID": 0,  
    "wWartosc": 0  // czas pracy
}  

### 16#20B - Zapisz całkowity czas jazdy kombajnu
**UWAGA: Ten rozkaz niedawno uległ zmianie w STRADA**  
>{  
    "rozkaz": "czasJazdy_20B",    // identyfikator dla Arka  
    "wID": 0,  
    "wWartosc": 0  
}  

### 16#20C - Zapisz całkowity dystans kombajnu
>{  
    "rozkaz": "calkDystKomb_20C",  
    "wWartosc": 0  
}  

### 16#216 - Zapisz kanał radiowy SSRK
>{  
    "rozkaz": "kanalSSRK_216",  
    "wWartosc": 0  
}  

### 16#21B - Zapisz typ skrawu wzorcowego
>{  
    "rozkaz": "typSkrawu_21B",  
    "wWartosc": 0  
}  

### 16#21C - Zapisz fazę skrawu wzorcowego
>{  
    "rozkaz": "fazaSkrawu_21C",  
    "wWartosc": 0  
}  


### 16#21D - Zapisz auto fazę skrawu wzorcowego
>{  
    "rozkaz": "autoFazaSkrawu_21D",  
    "wWartosc": 0  
}  

### 16#221 - Zapisz miejsce sterowania kombajnu przez zewnętrzny system sterowania
>{  
    "rozkaz": "zewnSystSter_221",  
    "wWartosc": 0  
}  

### 16#222 - Zapisz tryb pracy pomp hydrauliki
>{  
    "rozkaz": "trybHydr_222",  
    "wWartosc": 0  
}  

### 16#310 - Podaj status wejść/wyjść wybranego bloku
Dane diagnostyczne bloku.  
**UWAGA: Ten rozkaz niedawno uległ zmianie w STRADA**  
>{  
    "rozkaz": "statusWeWyBloku_310",  
    "wWartosc": 0,  // czas co jaki node ma odpytywać PLC
    "sID": 0,  // id bloku,   
}  

### 16#31A - Podaj dane wizualizacyjne dodatkowe
>{  
    "rozkaz": "daneWizDodatkowe_31A",  
    "wWartosc": 0  
}  


### 16#401 - Testuj hamulec
**Należy dodać obsługę odpowiedzi danych serwera.**  

Uwaga: operacja obsługiwana w sposób synchroniczny, jednak wymagane jest cykliczne odpytywanie, aby podtrzymać sterownik w trybie testu hamulców.  

Jeśli przez określony czas (domyślnie 500ms) Serwer nie otrzyma instrukcji 401, sterownik przerwie procedurę testu hamulców.  
>{  
    "rozkaz": "testHamulca_401",  
    "wWartosc": 0  
}  

### 16#402 - Sterowanie reflektorami
>{  
    "rozkaz": "sterReflektorami_402",  
    "wID": 0,  // reflektor id  
    "wWartosc": 0  // steruj  
}  

### 16#403 - Zeruj liczniki dzienne
*Rozkaz dla kombajnów ścianowych - chodnikowe mają swój osobny*  
>{  
    "rozkaz": "zerujLicznikiDzien_403"  
}  

### 16#404 - Kalibracja czujnika położenia napędów hydraulicznych
*Rozkaz dla kombajnów ścianowych - kalibracja enkoderów, chodnikowe mają osobny*  
>{  
    "rozkaz": "kalibracjeEnk_404",   
    "wID": 0,  // id napedu  
    "wWartosc": 0  // pozycja  
} 

### 16#520 - Elektroniczna Książka Serwisowa
>{  
    "rozkaz": "eks_520", // identyfikator dla Arka  
    "wActivID": 0 // identyfikator rozkazu (np 2001 dla prac miesięcznych)  
}

### 16#600 - Zapisz nazwę pliku Skrawu Wzorcowego wybranego przez użytkownika
>{  
    "rozkaz": "plikSkrawuWz_600",  
    "sWartosc": "string23"  // byte 0..23: [STRING(23)];  
}  

### 16#601 - Podaj nazwy plików Skrawu Wzorcowego
*Dodać obsługę danych zwrotnych z serwera*  
>{  
    "rozkaz": "podajNazwyPlikow_601",  
    "wWartosc": 0  
}

### 16#602 - Skasuj aktywny plik Skrawu Wzorcowego i usuń dane Skrawu z pamięci
>{   
    "rozkaz": "skasujAktywnyPlik_602"  
}  

### 16#603 - Podaj nazwę aktualnie wybranego pliku Skrawu Wzorcowego
*Dodać obsługę danych zwrotnych z serwera*  
>{  
    "rozkaz": "podajNazwePliku_603",  
    "wWartosc": 0  
}  

### 16#604 - Skasuj plik Skrawu Wzorcowego (inny niż aktywny)
>{  
    "rozkaz": "skasujPlik_604",  
    "sWartosc": "string23"  
} 

### 16#605 - Zmień nazwę pliku Skrawu Wzorcowego
>{  
    "rozkaz": "zmienNazwePliku_605",  
    "sNazwaPlikuOld": "string23",  
    "sNazwaPlikuNew": "string23"  
}  

### 16#606 - Stwórz nowy plik Skrawu Wzorcowego
>{  
    "rozkaz": "nowyPlik_606",  
    "sWarotsc": "string23"  
}  





# Nowe rozkazy
### Tryb serwisowy
Dostępne tylko dla kombajnu GUL.  
>{  
    "rozkaz": "trybSerwisowy_223",  
    "wWartosc": 0,  // 0-wyłącz; 1-włącz tryb serwisowy  
    "wID": 0   
}  

Gdzie *wID* przyjmuje takie same wartości jak w rozkazie 209.  

Rozkaz jest wysyłany cyklicznie co 3 sekundy aby podtrzymać tryb serwisowy.  
Po timeoucie sterownik powinien zautomatycznie go wyłączyć.  



# Rozkazy do opracowania
### Wgrywanie/zgrywanie plików 
Ten rozkaz nie będzie dodawany do STRADy.  
>{  
    "rozkaz": "zarzadzaniePlikami",  
    "sWartosc": "jsonNaPLC",  
}  

sWartość:  

- jsonNaPLC  
- jsonZPLC  
- jsonNaUsb     
- jsonZUsb    
- parametryNaPLC   
- parametryZPLC   
- eksNaPLC   
- eksZPLC   
- skrawNaPLC   
- skrawZPLC   


### Aktualizacja oprogramowania z pendrive
- Podać nazwę pliku (rar, zip) z paczką aktualizacyjną.  
- Kopiuj plik z pendrive na serwer.  
- Podać listę paczek z aktualizacjami już na serwerze.  
- Przywrócenie poprzedniej wersji.  

 














