# Windows
INSTRUKCJA NIEAKTUALNA!!!!!  
Po zmianach na serwerze jest nowy sposób jego uruchomienia.  

### Instalacja
- Zainstalować na laptopie oprogramowanie dla serwera ze strony [**link**](http://nodejs.org/).  
- Serwer Arka przegrać do katalogu D:\node.  
- Aktualną aplikacje wizualizacyjną przegrać do katalogu D:\node\www albo D:\node\build (w zależności od tego jaka opcja będzie w serwerze).  
- Uruchomić konsolę CMD i wpisywać po kolei komendy aby uruchomić serwer:  
`> d:`  
`> cd node`  
`> node tcp_cl3.js` - dobrze jest używać klawisza TAB (autouzupełnianie)
	
- W przeglądarce internetowej wpisać w pasku adresu:  
localhost:8888 - (Wiesiek port 8887)

### Dodatkowe uwagi
Podłączyć sterownik PLC do sieci w której znajduje się laptop.  

Laptop, przykładowe ustawienia sieci:   
IP: 192.168.3.66  
Maska: 255.255.255.0  

Nawigacja po wizualizacji odbywa się za pomocą klawiszy: góra, dół, lewo, prawo, enter, escape.

### Rozwiązywanie problemów
- Diagnostyka serwera: w przeglądarce wpisać adres localhost:8889.
- Bardziej szczegółowe logi z błędami serwera można uzyskać uruchamiając serwer z innego skryptu:   
`> d:`  
`> cd node`  
`> cd node`  
`> node main.js`  
- Błędy zgłaszane przez przeglądarkę można podejrzeć w konsoli:
    - PPM na oknie strony internetowej  
    - Inspect Element (w chrome)  
    - Wybrać zakładkę "Console" i sprawdzić logi

# Linux

in progress...