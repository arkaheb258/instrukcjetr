
# JQueryUI themes
### Alarm
Zakładka: Error  
Background color & texture: #cd0a0a (ten oryginalny), najbardziej wąskie ukośne paski, opacity 30%   
Border: #cd0a0a (ten oryginalny)  
Text: #ffffff (bialy)  
Icon: #ffffff (czarny)  

### Ostrzeżenie 
Zakładka: Highlight  
Background color & texture: #ffea00, najbardziej szerokie ukośne paski, opacity 50%    
Border: #ffea00  
Text: #000000 (czarny)  
Icon: #000000 (czarny)  
 
 
# Zmiany w bibliotece UI
### tabs
Wyłączono reakcje kontrolki TABS na klawisze arrow: down oraz up
>       //case keyCode.DOWN:  
        //case keyCode.UP:  

### accordion
Wylaczono reakcje kontrolki ACCORDION na klawisze arrow: left oraz right  
>          switch ( event.keyCode ) {
            //case keyCode.RIGHT:
            case keyCode.DOWN:
                //toFocus = this.headers[ ( currentIndex + 1 ) % length ];
                break;
            //case keyCode.LEFT:
            case keyCode.UP:
                //toFocus = this.headers[ ( currentIndex - 1 + length ) % length ];
                break;
            case keyCode.SPACE:
            case keyCode.ENTER:
                this._eventHandler( event );
                break;
            case keyCode.HOME:
                toFocus = this.headers[ 0 ];
                break;
            case keyCode.END:
                toFocus = this.headers[ length - 1 ];
                break;
        }
        
### menu
Wylaczono nawigacje z kontrolki menu (zmiany w dwoch miejscach):  
>       _keydown: function( event ) {
        var match, prev, character, skip,
            preventDefault = true;
        switch ( event.keyCode ) {
        case $.ui.keyCode.PAGE_UP:
            //this.previousPage( event );
            break;
        case $.ui.keyCode.PAGE_DOWN:
            //this.nextPage( event );
            break;
        case $.ui.keyCode.HOME:
            //this._move( "first", "first", event );
            break;
        case $.ui.keyCode.END:
            //this._move( "last", "last", event );
            break;
        case $.ui.keyCode.UP:
            //this.previous( event );
            break;
        case $.ui.keyCode.DOWN:
            //this.next( event );
            break;
        case $.ui.keyCode.LEFT:
            //this.collapse( event );
            break;
        case $.ui.keyCode.RIGHT:
            if ( this.active && !this.active.is( ".ui-state-disabled" ) ) {
                //this.expand( event );
            }
            break;
            // case $.ui.keyCode.ENTER:
            // case $.ui.keyCode.SPACE:
            // this._activate( event );
            // break;
        case $.ui.keyCode.ESCAPE:
            //this.collapse( event );
            break;
        default:
            preventDefault = false;
            prev = this.previousFilter || "";
            character = String.fromCharCode( event.keyCode );
            skip = false;
            clearTimeout( this.filterTimer );



 >           //        nested = item.children( ".ui-menu" );
            //        if ( nested.length && ( /^mouse/.test( event.type ) ) ) {
            //            this._startOpening(nested);
            //        }



 >               collapseAll: function( event, all ) {
            //        clearTimeout( this.timer );
            //        this.timer = this._delay(function() {
            //            // If we were passed an event, look for the submenu that contains the event
            //            var currentMenu = all ? this.element :
            //                $( event && event.target ).closest( this.element.find( ".ui-menu" ) );
            //
            //            // If we found no valid submenu ancestor, use the main menu to close all sub menus anyway
            //            if ( !currentMenu.length ) {
            //                currentMenu = this.element;
            //            }
            //
            //            this._close( currentMenu );
            //
            //            this.blur( event );
            //            this.activeMenu = currentMenu;
            //        }, this.delay );
                },

### selectmenu
>               case $.ui.keyCode.LEFT:
                    //this._move( "prev", event );
                    break;
                case $.ui.keyCode.RIGHT:
                    //this._move( "next", event );
                    break;








