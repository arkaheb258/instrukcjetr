



echo ondemand > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor  
echo 336000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq  
echo 1008000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq  
echo 40 > /sys/devices/system/cpu/cpufreq/ondemand/up_threshold        -    95  
echo 200000 > /sys/devices/system/cpu/cpufreq/ondemand/sampling_rate  




odpalenie serwera X tylko dla przeglądarki  
http://raspberrypi.stackexchange.com/questions/11866/how-can-i-start-x11-only-for-a-single-application   

http://raspberrypi.stackexchange.com/questions/12606/run-a-gui-without-the-desktop  

https://bbs.archlinux.org/viewtopic.php?id=83898  


###wireshark   
ip.addr==192.168.3.30   
ip.src==192.168.3.30  
ip.dst==192.168.3.30  
ip.src==192.168.3.30 and tcp.len>80  
ip.src==192.168.3.30 and tcp.port==20021  





- Wizualizacja  
    - [Instalacja - lokalnie](wewn/wizualizacja/readme.md)  
    Instalacja serwera oraz wizualizacji lokalnie na laptopie.  
    - [JQuerry](wewn/wizualizacja/readme2.md)  
    Zmiany w JQuerryUI themes.  
    - [Rozkazy www<->node](wewn/wizualizacja/readme3.md)  
    Opis komunikacji pomiędzy przeglądarą a serwerem.    

- [WES7 dla STD](wewn/STD/readme.md)  
Wskazówki do tworzenia systemu Windows Embedded Standard 7 od podstaw dla STD (PP500).  

- [TinyCore](wewn/tinycore/readme.md)  
Wskazówki do tworzenia systemu systemu linux TinyCore.  

- [Olimex](wewn/olimex/readme.md)  
Wskazówki do stworzenia od zera systemu operacyjnego na komputer Olimex.  

- [Magellan (WK5)](wewn/magellan/readme.md)  
Wskazówki do stworzenia systemu Xubuntu na komputer Magellan (WK5) od podstaw.  

- Linux  
    - [Komendy](wewn/linux/komendy.md)     
    Często wykorzystywane polecenia konsoli.  
    - [Bash](wewn/linux/bash.md)  
    Pisanie skryptów pod basha.  
    - [Inne](wewn/linux/inne.md)  
    Dodatkowe informacje, np struktura katalogów itp.  

- [Wydawka - Paweł](wewn/wydawka/wydawka.md)  
Procedura wydawki oprogramowania na kombajn. 

- [Temp](wewn/temp/readme.md)  
Pliki i notatki tymczasowe, nad którymi aktualnie trwają prace.  
Generalnie - śmietnik.  
