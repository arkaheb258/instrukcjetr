# Uwagi wstępne
System jest tworzony na laptopie z zainstalowanym systemem operacyjnym Ubuntu.  
Wystarczy karta microSD o pojemności 2GB.  
Większe pojemności nie są wskazane ponieważ przy tworzeniu klonu karty dla produkcji będzie miał on niepotrzebnie zwiększony rozmiar.

# To Do
- Nie da się kasować plików po sftp na koncie "olimex" - brak uprawnień
- Które rzeczy należy wyłączyć aby port szeregowy nie czekał na debug - to samo co Arek robił w beaglu
- Przeglądarka Midori - nie wyświetlają się wszystkie teksty z java script - prawdopodobnie problem systemu a nie przeglądarki.
- Błąd serwera Arka: ntp i hwclock


# Instalacja

### Pobranie obrazu systemu
Ściągnąć wersję minimalną systemu ze strony:  
[**link**](http://www.igorpecovnik.com/2014/11/18/olimex-lime-debian-sd-image/)


Jest to system bez środowiska graficznego, z minimalną ilością komponentów.
Wybrać wersję MICRO opartą na kernelu 3.4 debiana Wheezy lub Jessie - zawiera wszystkie potrzebne sterowniki.


### Nagranie systemu na kartę microSD
- Rozpakować ściągniętą paczkę dowolnym menadżerem archiwów.
- Włożyć kartę SD do laptopa, otworzyć konsolę i wpisać polecenie:  
`$ dmesg`  
Sprawdzić jakie oznaczenie dostała karta, powinno być "mmcblk0".
- Znawigować do katalogu w którym rozpakowaliśmy system, komendy do wykorzystania to:  
`$ cd ..`  
`$ cd nazwaKatalogu`  
 Przy wpisywaniu nazwy katalogu warto posługiwać się klawiszem TAB (autouzupełnianie).
- Odmontować kartę SD - PPM na ikonce karty microSD i wybranie opcji "Odmontuj"
- Nagrać obraz systemu na kartę microSD:  
`$ sudo dd if=Micro_Debian_1.5_wheezy_3.4.105.raw of=/dev/mmcblk0`
- Zakończenie kopiowania będzie zakomunikowane ilością przegranych bajtów, prędkością kopiowania itp.


### Pierwsze uruchomienie systemu
Sprawdzić działanie systemu i ustawić konto administratora.

- Odłączyć od Olimexa wyświetlacz LCD.      
W podstawowej konfiguracji monitor 10" nie jest obsługiwany.
- Umieścić karte microSD w olimexie i podać zasilanie. Powinna zaświecić się czerwona dioda a po kilku sekundach zielona.
- Podczas pierwszego uruchomienia system sam się konfiguruje (konto ssh itp).  
Trwa to kilkadziesiąt sekund po czym system sam się resetuje (obserować zieloną diodę)
- Komputer ma ustawione na początku dhcp.  
Należy podłączyć go do routera i sprawdzić jaki adres IP został przydzielony.  
Router u nas w biurze ma adres 192.168.1.1, 
user:"", pass:"automatyka".  
Sprawdzić adres w menu routera -> Status/Local Network/DHCP Clients Table.  
Olimex powinien mieć nazwę "micro"
- Zalogować się do systemu za pomocą programu putty za pomocą protokołu ssh:
login: "root", password: "1234"  
Od razu zostaniemy poproszeni o zmianę hasła, ustawić: "wihajster1234".  
Login i hasło są takie same do klienta sftp (czyli root/wihajster1234), najlepiej używać programu filezilla.


### Konfiguracja wyświetlacza LCD
Jeśli mamy wcześniej utworzony plik z konfiguracją to można przejść od razu do paragrafu *MAMY PLIK KONFIGURACYJNY* poniżej.  
Te czynności wykonujemy oczywiście na naszym stacjonarnym laptopie.

- Ściągnąć ze strony: [**link**](https://github.com/linux-sunxi/sunxi-boards/tree/master/sys_config/a20)  interesujący nas plik konfiguracyjny (np. a20-olinuxino_micro-lcd10.fex).
- Zainstalować na ubuntu zestaw narzędzi sunxi-tools, wszystkie operacje wykonać w konsoli na uprawnieniach administratora:  
`$ sudo su` - ustawienie uprawnień administratora  
`$ apt-get install libusb-1.0-0-dev` - pobranie i zainstalowanie biblioteki niezbędnej do działania sunxi-tools  
- Znawigować do katalogu gdzie chcemy ściągnąć pliki instalacyjne pakietu sunxi. Ja instalowałem do głównego "home"  
`$ git clone https://github.com/linux-sunxi/sunxi-tools` - pobranie pakietu  
`$ cd sunxi-tools` - wejście do katalogu  
`$ make` - kompilacja, po ukończeniu i wpisaniu komendy   
`$ ls` - pojawią się podświetlone na błękitno programy z których będziemy korzystać  
- Przegrać do katalogu "sunxi-tools" plik konfiguracyjny do wyświetlacza *.fex  
`$ ./fex2bin a20-olinuxino_micro-lcd10.fex micro.bin` - kompilacja pliku konfiguracyjnego, powstanie plik "micro.bin"
 
 *MAMY PLIK KONFIGURACYJNY*
 
- Zainstalować na laptopie midnight commander.  
`$ apt-get install mc`  
`$ sudo mc` - uruchomienie midnight commandera z uprawnieniami administratora
- Włożyć karte microSD z systemem do laptopa, za pomocą mc znawigować do karty microSD:  
/media/pk/micro/boot i zmienić nazwę "micro.bin" na "_micro.bin"
- Przegrać stworzony przez nas plik "micro.bin" do lokalizacji j.w. na karcie
- Włożyć kartę do olimexa,
- Podłączyć LCD (gniazdo na płytce pod tasiemkę jest opisane "LCD"), podać napięcie i sprawdzić czy matryca działa.


### Ewentualna modyfikacja poleceń dla kernela
Istnieje możliwość dodania nowych komend dla jądra podczas boot-a.  
Można dodać np "quiet" aby system nie sypał komunikatami podczas startu itp.

- Zainstalować z konsoli program mkimage:   
`$ apt-get install mkimage`  
- Skopiować do katalogu roboczego na laptopie z karty SD plik z poleceniami dla jądra.  
    Scieżka: /media/pk/micro/boot/boot.cmd.  
    Można też od razu zmienić nazwę jego odpowiednika binarnego na karcie "boot.scr" na "_boot.scr".  
    W edytorze tekstowym dodać interesujące nas nowe komendy, np "quiet".  
    UWAGA: komendy powinna oddzielać jedna spacja.
- W katalogu roboczym wpisać komendę "mkimage -C none -A arm -T script -d boot.cmd boot.scr"
- Przegrać "nowo utworzony "boot.scr" na karte


# Konfiguracja
Logujemy się w konsoli na konto root/wihajster1234


### Aktualizacja listy pakietów debiana  
`$ sudo apt-get update`


### Aktualizacja systemu
`$ sudo apt-get dist-upgrade`  
Odpowiedzi:  
- disable ssh password? - YES  
- /etc/init.d/rsyslog   - YES  

Na wszelkie zapytania czy zachować starszą wersję biblioteki/pliku nalezy odpowiadać opcją DEFAULT.
`$ reboot`  


### Instalacja midnight commandera  
`$ apt-get install mc` 


### Stworzenie nowego konta użytkownika  
`$ adduser zzm` - nadać hasło "wihajster1234"  /WAŻNE - tak ma być, to jest to samo co na DC1.   
`$ usermod -aG sudo zzm`  - nadanie uprawnień administratora.  
LUB:  
`$ visudo` - otworzy się edytor, pod uprawnieniami dla root wpisać nową linię dla użytkownika zzm:  
"zzm ALL=(ALL:ALL) ALL"   
następnie wyjść z edytora i zapisać zmiany (CTRL-X / Y / enter)



### Nadanie kilku komendom uprawnień roota
Trzeba użwyać komendu **sudo** ale nie trzeba podawać hasła.  
`$ sudo visudo`  

>zzm ALL=(ALL:ALL) ALL   
zzm ALL=NOPASSWD: /sbin/shutdown  
zzm ALL=NOPASSWD: /sbin/passwd    
zzm ALL=NOPASSWD: /bin/date    
zzm ALL=NOPASSWD: /usr/bin/node    

Potem można zrobić restart systemu za pomocą komeny:
`$ sudo shutdown -r now`  


### Dodanie graficznego systemu okien X.Org  
`$ apt-get -y install xorg`  
Opcja -y automatycznie odpowiada YES na wszystkie możliwe zapytania instalatora.  
Ustawić klawiaturę "English US".


### Dodanie menadżera okienek lightDM  
`$ apt-get -y install lightdm` 


### Dodanie środowiska graficznego z podstawowym pakietem aplikacji  
`$ apt-get -y install xfce4`


### Dodanie zestawu ikonek  
`$ apt-get -y install gnome-icon-theme`


### Zrestartować system  
`$ reboot`


### Zalogować się na konto użytkownika *zzm*, skonfigurować wstępnie wygląd pulpitu:
- przy zapytaniu o ustwienie panelu kliknąć "One empty panel"  
- PPM na pulpit Desktop Settings / Icons / odznaczyć wszystkie ikonki  
- PPM pasek panelu / Panel Preferences... / Items / + / dodać "Aplications Menu" oraz "Window Buttons"  
- PPM pasek panelu / Panel Preferences... / Display / zaznaczyć "Lock Panel", "Auto show and hide", suwakami zjechać na minimalne wartości
- reboot


### Autologowanie użytkownika zzm
- zalogować się oczywiście przy starcie systemu na użytkownika "zzm"
- uruchomić midnight commandera - w konsoli "sudo mc"
- znawigować do /etc/lightdm/ligthdm.conf -> wyedytować (F4 w mc), w pliku ważne są te dwie linie:  

>[SeatDefaults]  
autologin-user=zzm  
autologin-user-timeout=0 

- zapamiętać zmiany, reboot


### Wyłączenie sleep mode dla matrycy LCD  
`$ sudo mc`  
Znawigować do /etc/lightdm/ligthdm.conf -> F4 aby wyedytować  
Zmienić ustawienia:  
>[SeatDefaults]  
xserver-command=X -s 0 dpms  

Zapisać zmiany w pliku CTRL+X 


### Instalacja serwera nodejs, w konsoli:  
`$ sudo apt-get install nodejs`  
Jeśli pokaże się komunikat, że nie ma takiego repozytorium (debian wheezy), należy go zainstalować ze źródeł:  

znawigować w konsoli do katalogu domowego użytkownika zzm.   
`$ wget http://nodejs.org/dist/v0.12.0/node-v0.12.0.tar.gz` - pobranie paczki ze źródłami  
`$ tar -zxf node-v0.12.0.tar.gz` - rozpakowanie archiwum  
`$ cd node-v0.12.0` - wejście do katalogu  
`$ ./configure` - ustawienia dla polecenia make  
`$ make` - kompilacja źródeł  
`$ sudo make install` - zainstalowanie nodejs


### Instalacja przeglądarki 
`$ sudo apt-get install midori`  
`$ sudo apt-get install iceweasel`   


### Instalacja programu do symulacji klawiatury i myszki
`$ sudo apt-get install xdotool`


### Instalacja programu do chowania kursora myszki
`$ sudo apt-get install unclutter`


### Konfiguracja zegara rtc  
Przegrać skrypt **start_rtc** do katalogu /home/zzm.  
Nadać mu uprawnienia:
`$ chmod +x start_rtc`   
Podlinkować go do katalu /etc/init.d:  
`sudo ln /home/zzm/start_rtc.sh /etc/init.d/x_rtc`    
Zarejestrować go:  
`update-rc.d x_rtc defaults`  

Przegrać skrypt **getTime.sh** do katalogu kopex/node/scripts.  

`chmod a+w /sys/class/i2c-adapter/i2c-1/new_device`   


### Podkręcenie zegara procesora  
`sudo cpufreq-set --g performance`  - to nie działa po reboocie!   

Stworzyć plik:
`$ touch /etc/default/cpufrequtils`  , wpisać do niego:

> GOVERNOR="performance"  



### Ustawienie tapety  
Za pomocą filezilli przegrać plik z tapetą do /usr/share/xfce4/backdrops.  
PPM na pulpicie / Desktop settings... / Background / wybrać naszą tapete z listy


### Przegranie katalogu kopex (serwer i strona www) do /home/zzm/  
Przegrać katalog do  /home/zzm/.   
Dodatkowo do tej samej lokalizacji przegrać plik *autostart*  
Nadać uprawnienia plikowi *autostart*.  
`$ sudo chmod a+rwx autostart` - nadanie uprawnień naszemu skryptowi


### Ustawienie skryptu "start" do autostartu
- Applications Menu / Settings / Session and Startup
- Application Autostart / Add
- Name: skryptKopex, Command: /home/zzm/autostart
- Close
- Reboot


Dodatkowo nadać skryptom odpowiednie uprawnienia:  
Znawigować do kopex/scripts.  
`$ chmod +x *`  
`$ chmod +s *`  


### Instalacja dodatkowych bibliotek  
`$ sudo apt-get install ttf-dejavu` - czcionki.  
`$ sudo apt-get install libwebkitgtk-3.0-0` - to odpowiada za poprawne wyświetlanie tekstów na buttonach w przeglądarce midori.   
`$ sudo apt-get install ttf-liberation` - Arial i inne.  


### Instalacja serwera NTP  
`$ sudo apt-get install ntp`   
`$ sudo apt-get install ntpdate`   


### Instalacja obsługi skryptów ftp   
`$ sudo apt-get install ncftp`  


### Instalacja serwera vnc
`$ sudo apt-get install x11vnc`  


### Instalacja dos2unix   
`$ sudo apt-get install dos2unix`  - program do konwersji plików tekstowych z microsoftowskim znakiem zakończenia lini itp.   


### Instalacja edytora plików
`$ sudo apt-get install gedit`   



### Konfiguracja iceweasel
Tryb pełnoekranowy - załączyć klikając F11, po restracie przeglądarka będzie to pamiętała.  

Wyłącznie "crash recovery feature":  
W pasku adresu wpisać   
`about:config`  

`browser.sessionstore.resume_from_crash`  zmienić na:  
`browser.sessionstore.resume_from_crash user_set boolean false`  

`toolkit.startup.max_resumed_crashes`  ustawić na -1.   



W pliku ~/.mozilla/firefox/profiles.ini:  
>[General]   
export MOZ_CRASHREPORTER-DISABLE=1  


W pliku /usr/share/iceweasel/browser/defaults/preferences/firefox.js zmienić na:  
>lockPref("browser.sessionstore.resume_from_crash", false)   
>lockPref("toolkit.startup.max_resumed_crashes", -1)   

Skopiować ten plik do  
/usr/lib/iceweasel/defaults/pref  


W pliku /etc/iceweasel/pref/iceweasel.js dodać:  
>lockPref("browser.sessionstore.resume_from_crash", false)   
>lockPref("toolkit.startup.max_resumed_crashes", -1)   


### Ustawienie statycznego adresu IP  
`$ sudo mc`  
Zawigować do */etc/network/interfaces* -> klawisz F4 aby wyedytować.  
Zmienić ustawienia dla "Wired adapter #1".
Wykomentować dhcp:  
>*#iface eth0 inet dhcp*      

Dopisać następujące linijki dla ustawień statycznego IP, pod linijką z dhcp:
>*iface eth0 inet static*   
>*address 192.168.3.51*   
>*netmask 255.255.255.0*    
>*gateway 192.168.3.1*  

Reboot.  
Sprawdzić ping-iem poprawność ustawień 


### Wyłącznie startu niektórych modułów jądra  
W katalogu /etc/modprobe.d:  
- *alsa-base-blacklist.conf* - odkomentować wszystkie wpisy blacklist.  
- *alsa-base.conf* - zakomentować wszystkie wpisy.  
- *fbdev-blacklist.conf* - dodać na górze nowe wpisy:  
>blacklist rfcomm  
blacklist hidp  


### Sklonowanie stworzonego systemu
- włożyć kartę microSD do laptopa
- odmontować kartę  
- sklonować kartę  
`$ sudo dd if=/dev/mmcblk0 of=~/sysOlimex2GB.img`
    
    
### Zmiana uprawnień katalogu build  
Przypadek gdy katalog /biuld nie ma uprawnień i nie można aktualizować plików po sftp.  
Połączyć się programem putty (user:debian / pass:temppwd).   
Znawigować do katalogu kopex, można sprawdzić aktualne uprawnienia plików komendą `$ ls -l`  
`$ sudo chmod -R 777 build`  - nadanie wszystkich uprawnień   


### Dodatki
Uruchomić skrypt  
`$ /etc/bash.bashrc.custom`  - temperatura  
`$ sudo cpufreq-info`    - procesor  

### Readonly  
Weyedytować plik */etc/fstab*, dodać wpisy:  
>mmcblkk0p1 /boot   ext4    default,ro 0 0
mmcblkk0p1 /bin   ext4    default,ro 0 0
mmcblkk0p1 /sbin   ext4    default,ro 0 0
mmcblkk0p1 /usr   ext4    default,ro 0 0


### prevent fsck from running at boot
`$ sudo tune2fs -c -1 -i 0 /dev/mmcblk0p1`   


### Wyłączenie logów  
`$ /etc/init.d/rsyslog stop`  
`$ update-rc.d -f rsyslog remove`  - wyłączenie   

`$ update-rc.d -f rsyslog defaults`  - włączenie  


### Wyłączenie journala plików  
`$ tune2fs -O ^has_journal /dev/mmcblk0p1`  








