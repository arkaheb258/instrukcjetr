
# Spis treści  
### Linux 
- [bash](linux/bash.md)  
- [inne](linux/inne.md)  
- [komendy](linux/komendy.md)  

### Magellan  
- [magellan - tinycore](magellan/readme.md)  

### Olimex  
- [olimmex](olimex/readme.md)  

### Raspberry  
- [raspberry](raspberry/pi2.md)  

### STD  
- [std](std/readme.md)  

### Tinycore  
- [tinycore](tinycore/readme.md)  

### Wizualizacja  
- [jqueryThemes](wizualizacja/jqueryThemes.md)  
- [stradaServBrowser](wizualizacja/stradaServBrowser.md)  
- [uruchominieNaLaptpie](wizualizacja/uruchominieNaLaptpie.md)  

### Wydawka  
- [wydawka](wydawka/wydawka.md)  

### rozne  
- [git](rozne/git.md) 
- [temp](rozne/temp.md) 


